# TBS Tools

This repository outlines the instructions for using the prebuild tbs-tools container image or customizing the Docker file that can be used with the larger [Tanzu-Gitlab](https://gitlab.com/gitlab-com/alliances/vmware/sandbox/vmworld-2020-demo/spring-music) pipeline project that integrates with Tanzu Build Service.

## Prerequisites

* **None**, if you intend on using the prebuild image.
* **Docker**, if you're going to customize the image, or retag it, and push it to another repository.

## Summary of files

* **[tbs-tools](https://gitlab.com/gitlab-com/alliances/vmware/sandbox/tbs-tools/container_registry)**, a prebuilt image with kubectl, the kp (kpack) CLI, and the create-kubeconfig.sh script by default.

* **Dockerfile**, contains the code to build an image from scratch with kubectl, the kp (kpack) CLI, and the create-kubeconfig.sh script by default.

* **create-kubeconfig.sh**, a script that is baked into the prebuilt tbs-tools image. It is responsible for generating a kubeconfig file in the running container by injecting environment variables configured on the Variables page within the Settings page of the Gitlab project. All the necessary elements for the kpack CLI will use this configuration for communicating with the cluster where TBS is hosted.

<pre>
export KUBECONFIG=~/.kube/config
export CLUSTER_CA=${TBS_CLUSTER_CERTIFICATE_AUTHORITY}
export CLUSTER_ADDR=${TBS_CLUSTER_ADDRESS}
export CLUSTER_USER_CERT=${TBS_CLUSTER_USER_CERT}
export CLUSTER_USER_KEY=${TBS_CLUSTER_USER_KEY}

mkdir -p "$(dirname "${KUBECONFIG}")"

cat > "${KUBECONFIG}" << EOF
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: ${CLUSTER_CA}
    server: ${CLUSTER_ADDR}
  name: cluster
contexts:
- context:
    cluster: cluster
    user: user
  name: user@cluster
current-context: user@cluster
kind: Config
users:
- name: user
  user:
    client-certificate-data: ${CLUSTER_USER_CERT}
    client-key-data: ${CLUSTER_USER_KEY}
EOF
</pre>

## Custom build

1. Modify the Dockerfile accordingly
2. Login to registry.

	<pre>
	docker login registry.gitlab.com
	</pre>
	
3. In the directory of the Dockerfile, execute the following command.

	<pre>
	docker build -t registry.gitlab.com/<repository-name>/tbs-tools .
	</pre>
	
4. Push the image.

	<pre>
	docker push registry.gitlab.com/<repository-name>/tbs-tools
	</pre>
	
5. Confirm image was pushed.

	<pre>
	https://gitlab.com/<repository-name>/tbs-tools/container_registry
	</pre
	
6. To confirm the image has the necessary tools to interact with TBS, execute the following to shell into it.

	<pre>
	docker run -d -t registry.gitlab.com/<repository-name>/tbs-tools #this will keep the container running, otherwise, it will exit.
	docker exec -it <container-id> /bin/bash
	ls /usr/local/bin
	</pre>
	
**NOTE**: The above commands assume the same name used, tbs-tools.

